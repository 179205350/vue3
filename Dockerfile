FROM node:10.16.2-alpine


WORKDIR /user/src/app


COPY . .

RUN npm install -g cnpm
RUN cnpm install

EXPOSE 8080

CMD [ "npm" , "run" , "serve" ]
